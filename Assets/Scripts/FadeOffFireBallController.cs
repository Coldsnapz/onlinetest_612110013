﻿using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(PhotonTransformView))]
[RequireComponent(typeof(PhotonRigidbody2DView))]
public class FadeOffFireBallController : MonoBehaviourPun, IPunInstantiateMagicCallback
{
    private float countdown = 1.5f;
    public float velocity;
    private Vector2 randomPickedTraget;
    private bool TragetSet = false;
    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        Destroy(this.gameObject, 15.0f);
    }

    private void FixedUpdate()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        
        if (countdown > 0)
        {
            countdown -= Time.deltaTime;
        }
        else if(!TragetSet)
        {
            PlayerController[] temp = GameObject.FindObjectsOfType<PlayerController>();
            randomPickedTraget = new Vector2(temp[Random.Range(0, temp.Length)].transform.position.x,temp[Random.Range(0, temp.Length)].transform.position.y);
            TragetSet = true;
        }
        else
        {
            Debug.Log(randomPickedTraget);
            
            this.transform.position = Vector3.MoveTowards(this.transform.position, randomPickedTraget, velocity*2*Time.deltaTime);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerController>().ChangeHealth(-1);
            Destroy(this);
        }
        else if (other.gameObject.CompareTag("Ground"))
        {
            Destroy(this);
        }
    }

    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;

        PhotonNetwork.Destroy(this.gameObject);
    }
}
