﻿using System;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Unity.Mathematics;

public class CharacterSelectionManagerController : MonoBehaviourPun, IOnEventCallback
{
    public static CharacterSelectionManagerController Singleton;

    public Animator[] Animators;
    public Toggle[] Toggles;
    public Text RText;
    public int oldIPick = -1;
    public int nowIPick = 0;
    private bool loadingNext = false;
    private int totalReady = 0;

    public bool[] selectableChar;

    private const byte createChooseCharEvent = 1;
    private const byte createReadyCheckEvent = 2;

    public GameObject playerData;

    public void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void ChooseChar0(bool toggle)
    {
        nowIPick = 0;
        if (oldIPick != 0 && oldIPick > -1)
        {
            Toggles[oldIPick].enabled = true;
            Toggles[oldIPick].isOn = false;
            oldIPick = 0;
        }
        else
        {
            oldIPick = -1;
        }

        object[] content = new object[] {0, toggle};
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
        SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
        PhotonNetwork.RaiseEvent(createChooseCharEvent, content, raiseEventOptions, sendOptions);
    }

    public void ChooseChar1(bool toggle)
    {
        nowIPick = 1;
        if (oldIPick != 1 && oldIPick > -1)
        {
            Toggles[oldIPick].enabled = true;
            Toggles[oldIPick].isOn = false;
            oldIPick = 1;
        }
        else
        {
            oldIPick = -1;
        }

        object[] content = new object[] {1, toggle};
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
        SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
        PhotonNetwork.RaiseEvent(createChooseCharEvent, content, raiseEventOptions, sendOptions);

    }

    public void ChooseChar2(bool toggle)
    {
        nowIPick = 2;
        if (oldIPick != 2 && oldIPick > -1)
        {
            Toggles[oldIPick].enabled = true;
            Toggles[oldIPick].isOn = false;
            oldIPick = 2;
        }
        else
        {
            oldIPick = -1;
        }

        object[] content = new object[] {2, toggle};
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
        SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
        PhotonNetwork.RaiseEvent(createChooseCharEvent, content, raiseEventOptions, sendOptions);
    }

    public void ChooseChar3(bool toggle)
    {
        nowIPick = 3;
        if (oldIPick != 3 && oldIPick > -1)
        {
            Toggles[oldIPick].enabled = true;
            Toggles[oldIPick].isOn = false;
            oldIPick = 3;
        }
        else
        {
            oldIPick = -1;
        }

        object[] content = new object[] {3, toggle};
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
        SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
        PhotonNetwork.RaiseEvent(createChooseCharEvent, content, raiseEventOptions, sendOptions);
    }

    public void ChooseChar4(bool toggle)
    {
        nowIPick = 4;
        if (oldIPick != 4 && oldIPick > -1)
        {
            Toggles[oldIPick].enabled = true;
            Toggles[oldIPick].isOn = false;
            oldIPick = 4;
        }
        else
        {
            oldIPick = -1;
        }

        object[] content = new object[] {4, toggle};
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
        SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
        PhotonNetwork.RaiseEvent(createChooseCharEvent, content, raiseEventOptions, sendOptions);
    }

    public void ReadyCheck()
    {
        if (oldIPick != -1)
        {
            GameObject playerdatatemp = Instantiate(playerData, Vector3.zero, quaternion.identity);
            playerdatatemp.GetComponent<PlayerData>().dataToLoad = nowIPick;

            foreach (Toggle tg in Toggles)
            {
                tg.enabled = false;
                RText.text = "Waiting";
            }
        }

        object[] content = new object[] {totalReady + 1};
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
        SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
        PhotonNetwork.RaiseEvent(createReadyCheckEvent, content, raiseEventOptions, sendOptions);
    }

    public void Update()
    {
        Debug.Log(GameObject.FindObjectsOfType<PlayerData>().Length + " " + PhotonNetwork.PlayerList.Length);
        if (totalReady >= PhotonNetwork.PlayerList.Length && !loadingNext)
        {
            RText.text = "Loading...";
            Invoke("LoadBossLevel", 3);
            loadingNext = true;
        }
    }

    public void LoadBossLevel()
    {
        PhotonNetwork.LoadLevel("BossFightScene");
    }

private void Awake()
    {
        Singleton = this;
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

        if (eventCode == createChooseCharEvent)
        {
            object[] data = (object[]) photonEvent.CustomData;
            
            int selectedChar = (int) data[0];
            bool checkStatus = (bool) data[1];
            
            Animators[selectedChar].SetBool("IsSelected",checkStatus);
            
            Toggles[selectedChar].enabled = !checkStatus;
            if(oldIPick >-1)
            {Toggles[oldIPick].enabled = true;}
        }

        if (eventCode == createReadyCheckEvent)
        {
            object[] data = (object[]) photonEvent.CustomData;
            
            int Ready = (int) data[0];

            totalReady = Ready;
        }
    }
}
