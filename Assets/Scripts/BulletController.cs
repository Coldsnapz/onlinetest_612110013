﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(PhotonTransformView))]
[RequireComponent(typeof(PhotonRigidbody2DView))]
public class BulletController : MonoBehaviourPun, IPunInstantiateMagicCallback
{
    private int OwnerViewID = -1;

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        info.Sender.TagObject = this.gameObject;
        OwnerViewID = info.photonView.ViewID;

        if (!photonView.IsMine)
            return;

        Destroy(this.gameObject, 10.0f);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (other.gameObject.GetComponent<UFOController>() != null)
            {
                other.gameObject.GetComponent<UFOController>().ChangeHealth(-1);
            }
            else if(other.gameObject.GetComponent<MinionController>() != null)
            {
                other.gameObject.GetComponent<MinionController>().ChangeHealth(-1);
            }
        }

        if (photonView.IsMine)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;

        PhotonNetwork.Destroy(this.gameObject);
    }
}
