﻿using System;
using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(PhotonTransformView))]
[RequireComponent(typeof(PhotonRigidbody2DView))]
public class FireBallController : MonoBehaviourPun, IPunInstantiateMagicCallback
{
    public int BounceCount = 0;
    public Rigidbody2D rb;
    public Vector2 upForce;
    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        rb = this.gameObject.GetComponent<Rigidbody2D>();
        Destroy(this.gameObject, 15.0f);
    }

    public void Start()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        rb.AddForce(upForce, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            BounceCount--;
            if (BounceCount<0)
            {
                Destroy(this);
            }
        }
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerController>().ChangeHealth(-1);
            Destroy(this);
        }
    }

    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;

        PhotonNetwork.Destroy(this.gameObject);
    }
}
