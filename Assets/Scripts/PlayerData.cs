﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerData : MonoBehaviourPun
{
    public int dataToLoad;

    private void Awake()
    {
        this.gameObject.name = name + photonView.ViewID;
        DontDestroyOnLoad(this.gameObject);
    }
}
