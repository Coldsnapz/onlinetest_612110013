﻿using UnityEngine;

public class LaserController : MonoBehaviour
{
    private float countdown = 0.5f;
    public void Start()
    {
        if (countdown > 0) 
        { 
            Destroy(this.gameObject, 1.2f);
        }
    }

    private void FixedUpdate()
    {
        if (countdown > 0)
        {
            countdown -= Time.deltaTime;
        }
        else
        {
            this.GetComponentInChildren<SpriteRenderer>().color = Color.red;
            this.GetComponentInChildren<Collider2D>().enabled = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().ChangeHealth(-1);
        }
    }
}
