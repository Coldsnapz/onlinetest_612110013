﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using Random = UnityEngine.Random;

public class MatchMakingManager : MonoBehaviourPunCallbacks
{
    public GameObject _Button;
    public GameObject ForceStarter;
    public Text button;
    public Text status;

    private bool isMatchMaking;

    private void Start()
    {            
        ForceStarter.SetActive(false);
        if(!PhotonNetwork.IsConnected)
        {
            _Button.SetActive(false);
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to photon on " + PhotonNetwork.CloudRegion + " server.");
        PhotonNetwork.AutomaticallySyncScene = true;
        _Button.SetActive(true);
    }

    
    public void OnClickedMatching()
    {
        isMatchMaking = !isMatchMaking;
        if (isMatchMaking)
        {
            Matchmaking();
        }
        else
        {
            StopSearch();
        }
    }
    public void Matchmaking()
    {
        button.text = "Cancle";
        status.text = "Looking for players...";

        PhotonNetwork.JoinRandomRoom();
        Debug.Log("Match making...");
    }

    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            status.text = "You are host\nlooking for players...\n\n" + PhotonNetwork.CurrentRoom.PlayerCount + "/5";
        }
        else
        {


            status.text = "Looking for players...\n\n" + PhotonNetwork.CurrentRoom.PlayerCount + "/5";
        }
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Couldn't find room available, createing new room.");
        CreateNewRoom();
    }

    public void CreateNewRoom()
    {
        int randomRoomName = Random.Range(0, 5000);
        RoomOptions roomOptions = new RoomOptions()
        {
            IsVisible = true,
            IsOpen = true,
            MaxPlayers = 5
        };
        PhotonNetwork.CreateRoom("RoomName_" + randomRoomName, roomOptions);
        Debug.Log("Room created, waiting for another players");
        ForceStarter.SetActive(true);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        status.text = "You are host\nlooking for players...\n\n" + PhotonNetwork.CurrentRoom.PlayerCount + "/5";
        if (PhotonNetwork.CurrentRoom.PlayerCount == 5 && PhotonNetwork.IsMasterClient)
        {
            Debug.Log("Match making complete!");
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
            PhotonNetwork.LoadLevel("CharacterSelectionScene");
        }
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            status.text = "You are host\nlooking for players...\n\n" + PhotonNetwork.CurrentRoom.PlayerCount + "/5";
        }
    }

    public void ForceStart()
    {
        Debug.Log("Forced to start the match!");
        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.CurrentRoom.IsVisible = false;
        PhotonNetwork.LoadLevel("CharacterSelectionScene");
    }

    public void StopSearch()
    {
        button.text = "Matchmaking";
        status.text = "Welcome to my midterm project\n\nJintaphum Sungkird 612110013\n\nPress Matchmaking to start the game!!!";
        PhotonNetwork.LeaveRoom();
        Debug.Log("Stop searching for player, leaving the room.");
        ForceStarter.SetActive(false);
    }
}
