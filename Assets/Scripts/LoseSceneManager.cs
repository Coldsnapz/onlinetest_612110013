﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class LoseSceneManager : MonoBehaviourPun
{
    private void Awake()
    {
        if (PhotonNetwork.InRoom)
        {
            PhotonNetwork.LeaveRoom();
        }
    }

    public void Back()
    {
        PhotonNetwork.LoadLevel("MatchMakingScene");
    }
}
