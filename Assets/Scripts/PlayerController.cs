﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Photon.Realtime;
using Sirenix.OdinInspector;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerSetting
{
    public const string PLAYER_COLOR = "PlayerColor";
    
    public static Color GetColor(int colorChoice)
    {
        switch (colorChoice)
        {
            case 0: return Color.red;
            case 1: return new Color(1,1,1, 0.5f);
            case 2: return Color.white;
        }
        return Color.white;
    }    
}

[RequireComponent(typeof(PhotonTransformView))]
public class PlayerController : MonoBehaviourPunCallbacks, IPunObservable
{
    public int maxHealth = 3;
    private int currentHealth;
    public float walkSpeed = 2.5f;
    public float jumpHeight = 5f;
    public float maxInvincibleDurationAfterHit = 2;
    private float currentInvincibleTime;

    public Transform GroundCheckTransform;
    public float groundCheckRadius = 0.2f;

    private Transform tragetTransform;
    public LayerMask mouseAimMask;
    public LayerMask groundMask;

    private float inputMovement;
    private Animator animator;
    private Rigidbody2D rb;
    private bool isGrounded;
    private Camera mainCamera;

    [FoldoutGroup("Shooting", true, 1)] public Vector3 centerOffset;
    public float fireRate;
    public GameObject BulletPrefab;
    public int bulletDamage = 5;
    public float bulletForce = 20f;
    private float nextFire = 0;

    private bool invincible;

    void Start()
    {
        tragetTransform = GameObject.Find("Crosshair").transform;
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        mainCamera = Camera.main;
        currentHealth = maxHealth;
    }

    void Update()
    {
        if (!photonView.IsMine)
            return;

        inputMovement = Input.GetAxis("Horizontal");

        // Aim
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mouseAimMask))
        {
            tragetTransform.position = hit.point;
        }

        // Jump
        if (Input.GetButtonDown("Jump"))
        {
            if (isGrounded)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(Vector2.up * Mathf.Sqrt(jumpHeight * -1 * Physics.gravity.y), ForceMode2D.Impulse);
            }
        }

        // Fire
        if (Input.GetButton("Fire1"))
        {
            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
                Shoot(this.transform, tragetTransform, fireRate);
            }
        }
    }

    private void FixedUpdate()
    {
        // Ground Check
        isGrounded = Physics2D.OverlapCircle(GroundCheckTransform.position, groundCheckRadius, groundMask);

        if (photonView.IsMine)
        {
            if (currentInvincibleTime > maxInvincibleDurationAfterHit - 0.5f)
            {
                currentInvincibleTime -= Time.deltaTime;
                Hashtable props = new Hashtable
                {
                    {PlayerSetting.PLAYER_COLOR, 0}
                };
                PhotonNetwork.LocalPlayer.SetCustomProperties(props);
                invincible = true;
            }
            else if (currentInvincibleTime > 0)
            {
                currentInvincibleTime -= Time.deltaTime;
                Hashtable props = new Hashtable
                {
                    {PlayerSetting.PLAYER_COLOR, 1}
                };
                PhotonNetwork.LocalPlayer.SetCustomProperties(props);
                invincible = true;
            }
            else
            {
                Hashtable props = new Hashtable
                {
                    {PlayerSetting.PLAYER_COLOR, 2}
                };
                PhotonNetwork.LocalPlayer.SetCustomProperties(props);
                invincible = false;
            }
        }

        if (!photonView.IsMine)
            return;

        // Movement
        rb.velocity = new Vector2(inputMovement * walkSpeed, rb.velocity.y);
        Vector2 facingVector2 = Vector2.right;

        // Facing
        if (tragetTransform != null)
        {
            if (tragetTransform.position.x - transform.position.x > 0)
            {
                facingVector2 = Vector2.right;
                animator.SetFloat("Speed", rb.velocity.x / walkSpeed);
                transform.rotation =
                    Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 50);
            }
            else
            {
                facingVector2 = Vector2.left;
                animator.SetFloat("Speed", -rb.velocity.x / walkSpeed);
                transform.rotation =
                    Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 180, 0), Time.deltaTime * 50);
            }
        }

        float angle = Mathf.Abs(Vector2.SignedAngle(facingVector2, tragetTransform.position - transform.position));

        if (angle > 45)
        {
            animator.SetFloat("Angle", 3);
        }
        else if (angle > 15)
        {
            animator.SetFloat("Angle", 2);
        }
        else
        {
            animator.SetFloat("Angle", 1);
        }

        animator.SetBool("IsFiring", Input.GetMouseButton(0));
    }

    void Shoot(Transform PlayerTransform, Transform TragetTransform, float FireRate)
    {
        object[] data = {photonView.ViewID};

        Vector2 dir = Vector3.Normalize(TragetTransform.position - PlayerTransform.position) * 2;

        GameObject bullet = PhotonNetwork.Instantiate(this.BulletPrefab.name,
            PlayerTransform.position + centerOffset + (Vector3) dir * 2,
            Quaternion.identity,
            0,
            data);

        Rigidbody2D bulletrb = bullet.GetComponent<Rigidbody2D>();
        bulletrb.velocity = dir * bulletForce;
    }


    public void ChangeHealth(int amount)
    {
        if (!invincible)
        {
            if (amount < 0)
            {
                currentInvincibleTime = maxInvincibleDurationAfterHit;
            }

            currentHealth += amount;
            Debug.Log(this.name + "'s remaining hp = " + currentHealth);
            if (currentHealth <= 0)
            {
                if (photonView.IsMine)
                {
                    PlayerKnockedOut();
                }
            }
        }
    }

    public void PlayerKnockedOut()
    {
        Destroy(this);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentHealth);
        }
        else
        {
            currentHealth = (int) stream.ReceiveNext();
        }
    }
    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;
        
        PhotonNetwork.Destroy(this.gameObject);
    }
}
