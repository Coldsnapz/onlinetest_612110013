﻿using System;
using UnityEngine;
using Photon.Pun;
using Unity.Mathematics;

public class NetworkManager : MonoBehaviourPun
{
    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject[] GamePlayerPrefab;

    public GameObject UFOPrefab;
    private bool IsLoaded = false;

    private void Start()
    {
        SpawnPlayer();

        UFOController Boss = GameObject.FindObjectOfType<UFOController>();
        if (Boss == null)
        {
            PhotonNetwork.InstantiateRoomObject(UFOPrefab.name, UFOPrefab.transform.position, quaternion.identity,0);
        }
    }

    public void SpawnPlayer()
    {
        if (GameObject.FindObjectOfType<PlayerData>() != null)
        {
            GameObject _playerDataGO = GameObject.FindObjectOfType<PlayerData>().gameObject;
            PlayerData _playerData = _playerDataGO.GetComponent<PlayerData>();
            GameObject NewPlayer = PhotonNetwork.Instantiate(GamePlayerPrefab[_playerData.dataToLoad].name,
                new Vector3(0f, 0f, 0f),
                Quaternion.identity, 0);
            Destroy(_playerDataGO, 20);
        }
    }

    private void FixedUpdate() {
        if(PhotonNetwork.IsMasterClient != true)
            return;

        if (Time.timeSinceLevelLoad > 3)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                if (!IsLoaded)
                {
                    if (GameObject.FindObjectOfType<PlayerController>() == null)
                    {
                        IsLoaded = true;
                        //Destroy(this.gameObject);
                        PhotonNetwork.LoadLevel("LoseScene");
                    }

                    if (GameObject.FindObjectOfType<UFOController>() == null)
                    {
                        IsLoaded = true;
                        //Destroy(this.gameObject);
                        PhotonNetwork.LoadLevel("WinScene");
                    }
                }
            }
        }
    }
}
