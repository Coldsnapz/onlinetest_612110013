﻿using System;
using System.Collections;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

[RequireComponent(typeof(PhotonTransformView))]
public class PlayerNetController : MonoBehaviourPunCallbacks , IPunInstantiateMagicCallback
{

    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;

    private Player _player;
    

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
        }
        else
        {
            GetComponent<PlayerController>().enabled = false;
        }
    }

    private void Start()
    {
        PlayerSpriteSorting();
    }

    private void Update()
    {
        if (!photonView.IsMine)
            return;

    }

    private void PlayerSpriteSorting()
    {
        if (!photonView.IsMine)
        {
            SpriteRenderer[] spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer sr in spriteRenderers)
            {
                sr.sortingOrder -= 100 * photonView.ViewID;
            }
        }
    }
    
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (changedProps.ContainsKey(PlayerSetting.PLAYER_COLOR) &&
            targetPlayer.ActorNumber == photonView.ControllerActorNr)
        {
            object colors;
            if (changedProps.TryGetValue(PlayerSetting.PLAYER_COLOR, out colors))
            {
                SpriteRenderer[] sr = GetComponentsInChildren<SpriteRenderer>();
                foreach (var _sr in sr)
                {
                    _sr.color = PlayerSetting.GetColor((int)colors);
                }

                SpriteRenderer sr2 = gameObject.GetComponent<SpriteRenderer>();
                {
                    sr2.color = PlayerSetting.GetColor((int)colors);
                }
            }
            return;
        }
    }
}